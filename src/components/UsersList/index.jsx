import React from 'react';
import User from '../UserRow';
import { createUser, URL } from '../../models/User';

export default class UsersList extends React.Component{
    
    constructor() {
        super();
        
        this.state = {
            users: [],
            searcher: ''
        }
    }

    componentDidMount() {
        this.onAction('show');
    }

    onAction = (action, user) => {
        if(action === 'show'){
            fetch(`${URL}/users/`)
            .then(res => res.json())
            .then(data => {
                this.setState({users: data.map(user => createUser(user))});
            });
        } else {
            const users = [...this.state.users];
            if(users.includes(user)){
                users.splice(users.indexOf(user), 1);
                fetch(`${URL}/users/${user.id}`, { method: 'DELETE' })
                .then(res => res.json());
            }
            this.setState({ users })
        }
    }

    searcherUser = (e) => {
        this.setState({searcher: e.target.value});
    }
    
    submitSearch = (e, searcher) => {
        e.preventDefault();

        if(searcher !== ''){
            fetch(`${URL}/users?name=${searcher}`)
                .then(res => res.json())
                .then(users => {
                    this.setState({ users })
            });
        } else {
            this.onAction('show');
        }
    }

    render(){
        const usersList = this.state.users.map(user => <User key={ user.id } onAction={ this.onAction } user={ user }/>)
        return(
            <div className="container mt-5 mb-5 item-sliding-up">
                <div className="navbar">
                    <h3 className="text-center fw-lighter item-sliding-up">Users <strong>List</strong></h3>
                    <form onSubmit={(e) => this.submitSearch(e, this.state.searcher)} className="d-flex">
                        <input className="form-control me-2" onChange={(e) => this.searcherUser(e)} type="search" placeholder="Search user" aria-label="Search"/>
                        <button className="btn btn-outline-success" type="submit">Search</button>
                    </form>
                </div>
                <table className="table table-hover">
                    <thead>
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Email</th>
                        <th scope="col">Indirizzo</th>
                        <th scope="col">Azienda</th>
                        <th scope="col">Modifica</th>
                        </tr>
                    </thead>
                    <tbody>
                        { usersList }
                    </tbody>
                </table>
            </div>  
        );
    }
}