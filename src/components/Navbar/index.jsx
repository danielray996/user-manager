import React from 'react';
import logo from '../../logo.svg';
import { Link } from 'react-router-dom';

export default class Navbar extends React.Component{
    
    render(){
        return(
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container item-sliding-up">
                    <img src={logo} alt="logo" className="App-logo"/>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="d-flex">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <Link className="nav-link" to="/"><i className="fas fa-home"></i> Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/users"><i className="fas fa-users"></i> View users</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/insert_user"><i className="fas fa-user-plus"></i> Insert user</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}