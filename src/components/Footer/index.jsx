import React from 'react';
import epicode from '../../assets/Epicode.ico'

export default class Footer extends React.Component{
    
    render(){
        return(
            <div className="container-fluid border-top mt-5">
                <footer className="container d-flex flex-wrap justify-content-between align-items-center py-3 my-4">
                    <div className="col-md-4 d-flex align-items-center">
                        <span className="text-muted"><strong>Daniel Ray</strong> © 2021 Company, Inc</span>
                    </div>
                    <ul className="nav col-md-4 justify-content-end list-unstyled d-flex">
                        <li>
                            <a href="https://epicode.it/">
                                <img className="foot-icon" src={epicode} alt="epicode"/>
                            </a>
                        </li>
                    </ul>
                </footer>
            </div>
        );
    }
}