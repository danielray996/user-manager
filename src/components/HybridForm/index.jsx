import React from 'react';
import { withRouter } from 'react-router';
import { createUser, URL } from '../../models/User';

class HybridForm extends React.Component{
    
    constructor(){
        super();
        
        this.state = {
            user: {
                address: {},
                company: {}
            },
            success: false
        }
    }

    async componentDidMount(){
        if(this.props.match.params.id !== undefined){
            const user = await fetch(`${URL}/users/${this.props.match.params.id}`)
                .then(res => res.json());
            this.setState({ user: createUser(user) });
        }
    }

    async submitHandler (e, user, controlVar) {
        e.preventDefault();

        if(controlVar) {
            const results = await fetch(`${URL}/users/`,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(user)
            })
            if(results.status === 200) return true;
        } else {
            const results = await fetch(`${URL}/users/${user.id}`,{
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(user)
            })
            if(results.status === 200) { return true; }
        }
    }

    changeValue = (event, type) => {
        switch(type){
            case 'name':  
                this.setState({user: {...this.state.user, name: event.target.value}});
                break;
            case 'email': 
                this.setState({user: {...this.state.user, email: event.target.value}});
                break;
            case 'address': 
                this.setState({user: {...this.state.user, address: {...this.state.user.address, street: event.target.value }}});
                break;
            default: this.setState({user: {...this.state.user, company: {...this.state.user.company, name: event.target.value }}});
                     break;
        }
    }

    render(){
        const controlVar = this.props.successInsert ? true : false;
        const phrasesInsert = ['Inserisci un nuovo utente', 'Inserisci', 'Utente inserito con successo!'];
        const phrasesUpdate = ['Modifica dell\'utente con ID: ' + this.state.user.id, 'Salva modifiche', 'Dati modificati con successo!'];

        return(
            <div className="container-fluid">
                <h3 className="text-center fw-lighter item-sliding-up mt-5"><strong><i className="fas fa-user"></i> {controlVar ? phrasesInsert[0] : phrasesUpdate[0]}</strong></h3>
                <form onSubmit={(e) => this.setState({success: this.submitHandler(e, this.state.user, controlVar)})} className="container mt-2">
                    <div className="mb-3">
                        <label className="form-label"><strong>Nome</strong></label>
                        <input onChange={(e) => this.changeValue(e, 'name')} defaultValue={this.state.user.name} type="text" className="form-control" required/>
                    </div>
                    <div className="mb-3">
                        <label className="form-label"><strong>Email address</strong></label>
                        <input onChange={(e) => this.changeValue(e, 'email')} defaultValue={this.state.user.email} type="email" className="form-control" required/>
                        <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                    </div>
                    <div className="mb-3">
                        <label className="form-label"><strong>Indirizzo</strong></label>
                        <input onChange={(e) => this.changeValue(e, 'address')} defaultValue={this.state.user.address.street} type="text" className="form-control" required/>
                    </div>
                    <div className="mb-3">
                        <label className="form-label"><strong>Azienda</strong></label>
                        <input onChange={(e) => this.changeValue(e, 'company')} defaultValue={this.state.user.company.name} type="text" className="form-control" required/>
                    </div>

                    <div className="text-center">
                        <button className="btn btn-primary item-sliding-up" type="submit">{controlVar ? phrasesInsert[1] : phrasesUpdate[1]}</button>
                    </div>
                </form>
                { this.state.success && <h3 className="text-center text-success fw-lighter item-sliding-up mt-5"><strong>{controlVar ? phrasesInsert[2] : phrasesUpdate[2]}</strong></h3> }
            </div>
        );
    }
}

export default withRouter(HybridForm);