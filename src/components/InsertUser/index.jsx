import React from 'react';
import { withRouter } from 'react-router';
import HybridForm from '../HybridForm';

class InsertUser extends React.Component{

    constructor(){
        super();
        
        this.state = {
            successInsert: true
        }
    }

    render(){
        return(
            <HybridForm successInsert={ this.state.successInsert }></HybridForm>
        );
    }
}

export default withRouter(InsertUser);