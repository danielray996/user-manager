import React from 'react';
import { Link } from 'react-router-dom';

export default class UserRow extends React.Component{
    
    render(){
        return(
            <tr>
                <th scope="col">{ this.props.user.id }</th>
                <td>{ this.props.user.name }</td>
                <td>{ this.props.user.email }</td>
                <td>{ this.props.user.address.street }</td>
                <td>{ this.props.user.company.name }</td>
                <td>
                    <Link className="btn btn-warning me-2" to={'/user/' + this.props.user.id}>Edit</Link>
                    <button className="btn btn-danger me-2" onClick={() => this.props.onAction('del', this.props.user)}>Delete</button>
                </td>
            </tr>
        );
    }
}