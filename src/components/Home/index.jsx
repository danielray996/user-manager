import React from 'react';

export default class Home extends React.Component{
    
    render(){
        return(
            <div className="container mt-5 p-4 rounded-pill bg-image">
                <h3 className="text-center fw-lighter item-sliding-up">Welcome in the <strong>Users Manager</strong></h3>
            </div>
        );
    }
}