export class User {
    constructor(id, name, email, address, company){
        this.id = id;
        this.name = name;
        this.email = email;
        this.address = address;
        this.company = company;
    }
}

export function createUser(user){
    return new User(user.id, user.name, user.email, user.address, user.company);
}

export const URL = 'http://localhost:3001';