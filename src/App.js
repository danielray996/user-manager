import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
import Footer from './components/Footer';
import Home from './components/Home';
import Navbar from './components/Navbar';
import UsersList from './components/UsersList';
import InsertUser from './components/InsertUser';
import HybridForm from './components/HybridForm';

function App() {
  return (
    <BrowserRouter>
      <Navbar/>
        <Switch>
          <Route path="/" exact>
            <Home/>
          </Route>
          <Route path="/users">
            <UsersList/>
          </Route>
          <Route path="/user/:id">
            <HybridForm/>
          </Route>
          <Route path="/insert_user">
            <InsertUser/>
          </Route>
        </Switch>
      <Footer/>
    </BrowserRouter>
  );
}

export default App;
